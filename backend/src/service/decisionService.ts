import { Inject, Service } from "typedi";
import { IDecisionProvider } from "../external/dataProvider/decisionProvider/IDecisionProvider";
import { BusinessDetail } from "../models/businessDetails";

@Service()
export class DecisionService {
  @Inject()
  decisionProvider: IDecisionProvider;

  requestDecision(businessDetails: BusinessDetail, preAssessmentValue: number): boolean {
    return this.decisionProvider.requestDecision(businessDetails, preAssessmentValue)
  }
}