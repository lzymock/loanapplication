import { Service } from "typedi";
import { BalanceSheet } from "../models/balanceSheet";
import 'dotenv/config'

@Service()
export class LoanService {
    getPreAssessmentValue(balanceSheet: BalanceSheet[], loanAmount: number): number {
        const defaultPreAssessValue: number = +(process.env.DEFAULT_PREASSESSMENT ?? 20);
        const total = balanceSheet.length;
        if (total == 0) {
            return defaultPreAssessValue;
        }
        const numValid = balanceSheet.filter(sheet => sheet.assetsValue > loanAmount).length;
        return (numValid / total) * 100;
    }
}