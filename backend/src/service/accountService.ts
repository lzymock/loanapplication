import { Inject, Service } from "typedi";
import { BalanceSheet } from "../models/balanceSheet";
import { IAccountProvider } from "../external/dataProvider/accountingProvider/IAccountProvider";
import { accountProviderEnum } from "../constants/accountProviderEnum";

@Service()
export class AccountService {
  @Inject()
  accountProvider: IAccountProvider;
    
  async requestBalanceSheet(provider: accountProviderEnum): Promise<BalanceSheet[]> {
    return this.accountProvider.requestBalanceSheet(provider);
  }
}