import { Inject, Service } from "typedi";
import { LoanService } from "../service/loanService";
import { DecisionService } from "../service/decisionService";
import { BalanceSheet } from "../models/balanceSheet";
import { AccountService } from "../service/accountService";
import { BusinessDetail } from "../models/businessDetails";
import { accountProviderEnum } from "../constants/accountProviderEnum";
import { Logger } from "../external/logger";

@Service()
export class LoanHandler {
  @Inject()
  loanService: LoanService;
  
  @Inject()
  accountService: AccountService;

  @Inject()
  decisionService: DecisionService;

  @Inject()
  logger: Logger;

  initiateApplication() {
    return 'initiate Appplication success';
  }

  async requestOutcome(businessDetail: BusinessDetail, provider: accountProviderEnum, loanAmount: number) {
    try {
      const balanceSheet: BalanceSheet[] = await this.accountService.requestBalanceSheet(provider);
      const preAssessmentValue = this.loanService.getPreAssessmentValue(balanceSheet, loanAmount);
      
      return this.decisionService.requestDecision(businessDetail, preAssessmentValue);
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
}