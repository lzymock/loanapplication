import { Inject, Service } from "typedi";
import { AccountService } from "../service/accountService";
import { accountProviderEnum } from "../constants/accountProviderEnum";
import { Logger } from "../external/logger";

@Service()
export class AccountHandler {
  @Inject()
  accountService: AccountService;

  @Inject()
  logger: Logger;

  fetchBalanceSheet(provider: accountProviderEnum) {
    try {
      return this.accountService.requestBalanceSheet(provider);
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
}