import "reflect-metadata";
import 'dotenv/config'
import { Rest } from "./rest";
import { MockAccountingSoftware } from "../external/dataProvider/accountingProvider/mockAccountingSoftware";
import Container from "typedi";
import { IAccountProvider } from "../external/dataProvider/accountingProvider/IAccountProvider";
import { MockDecisionSoftware } from "../external/dataProvider/decisionProvider/mockDecisionSoftware";
import { IDecisionProvider } from "../external/dataProvider/decisionProvider/IDecisionProvider";


Container.set(IAccountProvider, new MockAccountingSoftware());
Container.set(IDecisionProvider, new MockDecisionSoftware());

//export const ACCOUNT_SOFTWARE_BEAN = new Token<IAccountProvider>('IAccountingProvider');
//Container.set(ACCOUNT_SOFTWARE_BEAN, AccountingSoftware);

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});
//const rest: Rest = new Rest();
try {
    const rest: Rest = Container.get(Rest);
    rest.init();
} catch (e) {
    console.log(e);
}