import { Server, Request } from "@hapi/hapi";
import { Inject, Service } from "typedi";
import 'dotenv/config'
import { LoanHandler } from "../../handlers/loanHandler";
import { AccountHandler } from "../../handlers/accountHandler";
import { FetchBalanceSheetSchema } from "../../validator/fetchBalanceSchema";
import { RequestOutComeSchema } from "../../validator/requestOutcomeSchema";
import { FetchBalanceSheetPayload } from "../../models/fetchBalanceSheetPayload";
import { RequestOutcomePayload } from "../../models/requestOutcomePayload";
import { Logger } from "../../external/logger";

@Service()
export class Rest {
  @Inject()
  loanHandler: LoanHandler;

  @Inject()
  accountHandler: AccountHandler;

  @Inject()
  logger: Logger;
    
  init = async () => {
    const server: Server = new Server({
      port: process.env.PORT?? 3000,
      host: 'localhost',
      routes: {
        "cors": {
          "origin": ["*"]
        }
      }
    });
    server.route({
      method: 'POST',
      path: '/InitiateApplication',
      handler: () => {
          return this.loanHandler.initiateApplication();
      }
    });
    server.route({
      method: 'POST',
      path: '/FetchBalanceSheet',
      handler: (request: Request) => {
        const payload = request.payload as FetchBalanceSheetPayload;
        return this.accountHandler.fetchBalanceSheet(payload.provider);
      },
      options: {
        validate: {
          payload: FetchBalanceSheetSchema
        }
      }
    });
    server.route({
      method: 'POST',
      path: '/RequestOutcome',
      handler: (request: Request) => {
        const payload = request.payload as RequestOutcomePayload;
        return this.loanHandler.requestOutcome(payload.businessDetail, payload.provider, payload.loanAmount);
      },
      options: {
        validate: {
          payload: RequestOutComeSchema
        }
      }
    });
    await server.start();
    console.log('Server running on %s', server.info.uri);
  }
}