import Joi from "joi"
import { accountProviderEnum } from "../constants/accountProviderEnum";

export const RequestOutComeSchema = Joi.object().keys({
    loanAmount: Joi.number(),
    provider: Joi.string().valid(...Object.values(accountProviderEnum)),
    businessDetails: Joi.object().keys({
        year: Joi.number(),
        name: Joi.string()
    })
});