import Joi from "joi"
import { accountProviderEnum } from "../constants/accountProviderEnum";

export const FetchBalanceSheetSchema = Joi.object().keys({
  provider: Joi.string().valid(...Object.values(accountProviderEnum)),
});