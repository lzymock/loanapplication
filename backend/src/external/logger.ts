/* eslint-disable @typescript-eslint/no-explicit-any */
import { Service } from "typedi";

@Service()
export class Logger { 
    info(message: any) {
        console.log(message);
    }
    warn(message: any) {
        console.log(message);
    }
    error(message: any) {
        console.log(message);
    }
}