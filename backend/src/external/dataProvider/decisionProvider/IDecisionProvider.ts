/* eslint-disable @typescript-eslint/no-unused-vars */
import { Service } from "typedi";
import { BusinessDetail } from "../../../models/businessDetails";

@Service()
export class IDecisionProvider {
    requestDecision(_businessDetails: BusinessDetail, _preAssessmentValue: number): boolean { throw Error('Not Defined'); }
}