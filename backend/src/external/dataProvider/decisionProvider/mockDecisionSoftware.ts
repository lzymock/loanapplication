import { Service } from "typedi";
import { IDecisionProvider } from "./IDecisionProvider";
import { BusinessDetail } from "../../../models/businessDetails";

@Service()
export class MockDecisionSoftware extends IDecisionProvider {
    requestDecision(_businessDetails: BusinessDetail, _preAssessmentValue: number): boolean {
        return _preAssessmentValue>60;
    }
}