/* eslint-disable @typescript-eslint/no-unused-vars */
import { Service } from "typedi";
import { BalanceSheet } from "../../../models/balanceSheet";
import { accountProviderEnum } from "../../../constants/accountProviderEnum";

@Service()
export class IAccountProvider {
    async requestBalanceSheet(provider: accountProviderEnum): Promise<BalanceSheet[]> { throw Error('Not Defined'); }
}