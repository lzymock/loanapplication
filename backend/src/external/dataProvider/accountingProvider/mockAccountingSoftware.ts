/* eslint-disable @typescript-eslint/no-unused-vars */
import { Service } from "typedi";
import { IAccountProvider } from "./IAccountProvider";
import { BalanceSheet } from "../../../models/balanceSheet";
import { accountProviderEnum } from "../../../constants/accountProviderEnum";

@Service()
export class MockAccountingSoftware implements IAccountProvider {
    async requestBalanceSheet(provider: accountProviderEnum): Promise<BalanceSheet[]> {
        const balanceSheetResponse: BalanceSheet[] = [];
        balanceSheetResponse.push(new BalanceSheet(2022, 9, -187000, 223452));
        balanceSheetResponse.push(new BalanceSheet(2022, 10, 2500, 22345));
        balanceSheetResponse.push(new BalanceSheet(2022, 11, 1150, 5789));
        balanceSheetResponse.push(new BalanceSheet(2022, 12, 250000, 1234));
        return Promise.resolve(balanceSheetResponse);
    }
}