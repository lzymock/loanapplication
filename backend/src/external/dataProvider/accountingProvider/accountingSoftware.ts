import { Service } from "typedi";
import { IAccountProvider } from "./IAccountProvider";
import { BalanceSheet } from "../../../models/balanceSheet";
import { accountProviderEnum } from "../../../constants/accountProviderEnum";
import { XEROAccountProvider } from "./externalProvider/xeroAccountProvider";
import { IProvider } from "./externalProvider/IProvider";

@Service()
export class AccountingSoftware implements IAccountProvider {
    async requestBalanceSheet(provider: accountProviderEnum): Promise<BalanceSheet[]> {
        const extProvider: IProvider = this.getProvider(provider);
        return extProvider.requestBalanceSheet();
    }

    getProvider(provider: accountProviderEnum): IProvider {
        switch (provider) {
            case accountProviderEnum.XERO:
                return new XEROAccountProvider();
            case accountProviderEnum.MYOB:
                throw Error('Not Implemented');
            default:
                throw Error('Not Implemented');
        }
    }
}