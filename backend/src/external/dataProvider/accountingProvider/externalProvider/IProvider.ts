import { BalanceSheet } from "../../../../models/balanceSheet";

export interface IProvider {
    requestBalanceSheet(): Promise<BalanceSheet[]>;
}