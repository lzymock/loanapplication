import { Service } from "typedi";
import { BalanceSheet } from "../../../../models/balanceSheet";
import { IProvider } from "./IProvider";
import axios from "axios";

@Service()
//Test class to check external dependency with different providers
export class XEROAccountProvider implements IProvider {
    async requestBalanceSheet(): Promise<BalanceSheet[]> {
        const response = await axios.post('http://localhost:3000/FetchBalanceSheet', {
            provider: 'XERO'
        })
        return response.data;
    }
}