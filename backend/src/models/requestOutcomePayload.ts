import { accountProviderEnum } from "../constants/accountProviderEnum";
import { BusinessDetail } from "./businessDetails";

export class RequestOutcomePayload {
    loanAmount: number;
    provider: accountProviderEnum;
    businessDetail: BusinessDetail;
}