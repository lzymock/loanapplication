import { accountProviderEnum } from "../constants/accountProviderEnum";

export class FetchBalanceSheetPayload {
    provider: accountProviderEnum;
}