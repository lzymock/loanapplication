export class BalanceSheet {
    year: number;
    month: number;
    profitOrLoss: number;
    assetsValue: number;
    constructor(year: number, month: number, profitOrLoss: number, assetsValue: number) {
        this.year = year;
        this.month = month;
        this.profitOrLoss = profitOrLoss; 
        this.assetsValue = assetsValue;
    }
}
